# Author: Laura Lindzey
# Copyright 2020-2022 University of Washington Applied Physics Laboratory
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.

"""
Utilities for calibrating the imaging sonar
"""

import cv2
import rosbag

import numpy as np
import scipy
import scipy.optimize

import charuco_utils.charuco_utils as charuco


def generate_sensor_pairs(
    bagfile, camera_info_topic, camera_image_topic, sonar_image_topic
):
    """
    Yield tuples of corresponding sensor data:
    sonar image, camera info, image mono

    Does not do any sort of message timestamp checking to ensure that
    we've optimally paired messages ... the other three are matched to
    the sonar imaged based on the order that they were recorded.
    """
    bag = rosbag.Bag(bagfile)
    sonar_image = None
    camera_image = None
    camera_info = None

    camera_count = 0
    camera_info_count = 0
    sonar_count = 0
    # None of the timestamps match up perfectly (even the ones that should ....)
    # So we're just going to grab the first of each other type that comes after
    # the sonar_image.
    for topic, msg, _tt in bag.read_messages():
        if topic == sonar_image_topic:
            sonar_count += 1
            # Check if we have a valid tuple; if so, yield it. Otherwise, discard

            if sonar_image is not None:
                if camera_image is not None and camera_info is not None:
                    yield (sonar_image, camera_info, camera_image)
                else:
                    print("Discarding sonar_image b/c no camera_info")
            # Re-initialize new set of data
            sonar_image = msg
            camera_info, camera_image = None, None
        elif topic == camera_info_topic:
            camera_info_count += 1
            if sonar_image is not None and camera_info is None:
                camera_info = msg
        elif topic == camera_image_topic:
            camera_count += 1
            if sonar_image is not None and camera_image is None:
                camera_image = msg

    # Handle potential last set in the bagfile
    if sonar_image is not None:
        if camera_image is not None and camera_info is not None:
            yield (sonar_image, camera_info, camera_image)


def get_black_squares(board):
    """
    Return coordinates (in meters) of the center of every black square
    on the input charuco board

    Assumes that top-left square is an aruco tag
    Black squares are numbered in row-major order, starting with 0
    """
    ncols, nrows = board.getChessboardSize()
    ss = board.getSquareLength()
    ii = 0
    centers = {}
    for row in range(nrows):
        if row % 2 == 0:
            cols = range(1, ncols, 2)
        else:
            cols = range(0, ncols, 2)
        for col in cols:
            xx = (col + 0.5) * ss
            yy = nrows * ss - (row + 0.5) * ss
            centers[ii] = (xx, yy)
            ii += 1

    return centers


# TODO: If we ever use the non-default board, need to add board as parameter
def init_charuco_sonar():
    aruco_dict, charuco_board = charuco.get_charuco_board("default_11x8_45mm")

    # If the charuco board is of a different shape, then our assumptions about
    # where the targets are will be wrong.
    ncols, nrows = charuco_board.getChessboardSize()
    assert 11 == ncols
    assert 8 == nrows

    # The black squares are numbered 0-N, in row-major order starting from
    # the top left corner of the charuco board. Each sonar target is a bolt
    # in the center of a square.
    black_squares = get_black_squares(charuco_board)
    sonar_targets = {'A1': 0, 'A2': 1, 'A3': 5, 'A4': 6, 'A5': 11,
                     'B1': 3, 'B2': 4, 'B3': 9, 'B4': 10, 'B5': 15,
                     'C1': 27, 'C2': 28, 'C3': 33, 'C4': 38, 'C5': 39,
                     'D1': 31, 'D2': 32, 'D3': 37, 'D4': 42, 'D5': 43}  # fmt: skip
    sonar_coords = {label: black_squares[ss] for label, ss in sonar_targets.items()}

    return aruco_dict, charuco_board, sonar_coords


def get_sonar_target_correspondences(labeled_points):
    """
    Find corresponding points in sonar and target frames, using the
    label to look up target coords.

    Inputs:
    * labeled_points: dict mapping label to (angle_deg, pt_range) tuple

    # TODO: Would be good to stop passing these around as tuples/lists/etc, and actually have a class.
    Returns:
    * sonar_points: locations of labeled points in the sonar frame
    * target_points: locations of labeled points in the target's frame.
    """
    _, _, sonar_coords = init_charuco_sonar()

    # I'm sure there's a better way to do this, but I'm blanking on it
    angles = []
    ranges = []
    target_points = []
    for label, (angle_deg, pt_range) in labeled_points.items():
        angles.append(np.radians(angle_deg))
        ranges.append(pt_range)
        coord = sonar_coords[label]
        target_points.append([coord[0], coord[1], 0])

    # angles = np.array([np.radians(nn) for nn, _ in points.values()])
    # ranges = np.array([nn for _, nn in points.values()])
    sonar_points = np.array([angles, ranges])
    target_points = np.transpose(np.array(target_points))
    return sonar_points, target_points


def image_from_sonar3d(points):
    """
    Project 3D points in the sonar's frame to the (R, theta) data returned
    by the sensor.

    Input parameters:
    points -- (xx, yy, zz) Coordinates of point to be transformed;
              ndarray with shape (3,N)

    Output:
    angles, ranges -- Polar coordinates of point in the sonar image;
              ndarray with shape (2,N)
    """
    xx = points[0, :]
    yy = points[1, :]
    zz = points[2, :]
    ranges = np.sqrt(xx * xx + yy * yy + zz * zz)
    angles = np.arctan2(yy, xx)
    return np.array([angles, ranges])


def estimate_target_translation(
    target_points,
    sonar_points,
    range_resolution,
    angle_resolution,
    rvec,
    initial_tvec,
    verbose=True,
):
    err0 = calc_projection_error(
        target_points,
        sonar_points,
        rvec,
        initial_tvec,
        range_resolution,
        angle_resolution,
    )
    if verbose:
        print("Initial error: {}".format(err0))

    fn = lambda x: calc_projection_error(  # noqa: E731
        target_points, sonar_points, rvec, x, range_resolution, angle_resolution
    )
    opt = {"maxiter": 3000}
    res = scipy.optimize.minimize(fn, initial_tvec, method="Nelder-Mead", options=opt)
    if res.status != 0 or res.fun > 100:
        print(
            "Optimization did not terminate successfully, or error too large. Trying 2nd pass"
        )
        res = scipy.optimize.minimize(fn, res.x, method="Nelder-Mead", options=opt)

    tvec = np.reshape(res.x, (3, 1))
    err_n = calc_projection_error(
        target_points, sonar_points, rvec, tvec, range_resolution, angle_resolution
    )
    if verbose:
        print("Final error: {}".format(err_n))

    return res.fun, tvec


def estimate_target_pose(
    target_points,
    sonar_points,
    range_resolution,
    angle_resolution,
    initial,
    verbose=True,
):
    # Initialize the rotation s.t. the target's frame is aligned
    # with the sonar's frame; this helps avoid falling into a local
    # minimum with the target normal pointed away from the sonar
    # TODO: However, it would be better to actually put constraints on
    #    the optimization to enforce valid bounds on the rotation vector:
    #    (1) magnitude <= 2pi, (2) normal must be pointing towards the sonar.
    #    * 1 is probably best done by after-the-fact normalization
    #      (bounds would just create local extrema along the bounds)
    #    * 2 is ???
    rvec = initial[0:3]
    tvec = initial[3:6]
    err0 = calc_projection_error(
        target_points, sonar_points, rvec, tvec, range_resolution, angle_resolution
    )
    if verbose:
        print("Initial error: {}".format(err0))
        # print("(Using rvec = {}, tvec = {}".format(rvec, tvec))

    fn = lambda x: calc_projection_error(  # noqa: E731
        target_points, sonar_points, x[0:3], x[3:6], range_resolution, angle_resolution
    )
    opt = {"maxiter": 3000}
    res = scipy.optimize.minimize(fn, initial, method="Nelder-Mead", options=opt)
    if res.status != 0 or res.fun > 100:
        print(
            "Optimization did not terminate successfully, or error too large. Trying 2nd pass"
        )
        res = scipy.optimize.minimize(fn, res.x, method="Nelder-Mead", options=opt)

    rvec = np.reshape(res.x[0:3], (3, 1))
    tvec = np.reshape(res.x[3:6], (3, 1))
    err_n = calc_projection_error(
        target_points, sonar_points, rvec, tvec, range_resolution, angle_resolution
    )
    if verbose:
        print("Final error: {}".format(err_n))

    return res.fun, rvec, tvec


def calc_projection_error(
    target_points, sonar_points, rvec, tvec, range_resolution, angle_resolution
):
    """
    Calculate the sum-squared reprojection error between corresponding points in the
    target frame and in the sonar image, given the input transform.

    Returns sum-squared error in pixels, using range/angle resolution
    to convert from the SI units reportd by the sonar to pixels.

    Input parameters:
    target_points -- (3,N) np.ndarray. points in the target's frame
                     (will usually be planar, but not required!)
    sonar_points -- (2,N) np.ndarray. Corresponding points in the sonar image.
                   1st row angles, 2nd is ranges. (in radians, meters, NOT pixels)
    rvec, tvec -- rotation and transformation from the sonar frame
                 (x fwd, y right, z down) to the target frame
    range_resolution -- (in meters)
    angle_resolution -- (in radians)
    """
    rvec = np.reshape(rvec, (3, 1))
    tvec = np.reshape(tvec, (3, 1))
    rot, _ = cv2.Rodrigues(rvec)

    # Calculate target point locations in image, in meters/radians
    target_sonar_frame = tvec + rot @ target_points
    # print("Targets, in sonar frame: ", target_sonar_frame)
    target_image_frame = image_from_sonar3d(target_sonar_frame)
    # print("Targets, in image coordinates: ", target_image_frame)

    # Calculate the reprojection error, in pixels
    d_angle = (sonar_points[0, :] - target_image_frame[0, :]) / angle_resolution
    # print("Angle errors: ", d_angle)
    d_range = (sonar_points[1, :] - target_image_frame[1, :]) / range_resolution
    # print("Range errors: ", d_range)
    err = np.sum(np.sqrt(d_angle * d_angle + d_range * d_range))
    return err


def get_sonar_resolution(sonar_image):
    # type: marine_acoustic_msgs.SonarImage -> (Float, Float)
    """
    ONLY USED for converting angle errors into pixels.

    The sonar has constant range resolution, but angle-dependent
    azimuth resolution. For now, just take average.

    * range_resolution
    * angle_resolution: in radians
    """
    rr = np.array(sonar_image.ranges)
    dr = rr[1:] - rr[0:-1]
    aa = np.array(sonar_image.azimuth_angles)
    da = aa[1:] - aa[0:-1]
    range_resolution = np.mean(dr)
    angle_resolution = np.mean(da)

    return range_resolution, angle_resolution


def calibrate_sonar(
    sonar_points,
    camera_points,
    range_resolution,
    angle_resolution,
    init_rvec=None,
    init_tvec=None,
    verbose=True,
):
    # Initialize the sonar to be aligned with camera axis.
    # There's a rotation here because the camera has
    # X-right, but sonar is X-fwd. rvec is the Rodrigues representation
    # of the rotation required to rotate the sonar
    # frame with the camera, since we are solving for the transform
    # for X_camera = tvec + rot @ X_sonar
    # magnitude = 2*pi/3 about the [1,1,1] vector.
    if init_rvec is None:
        init_rvec = (1.2092, 1.2092, 1.2092)
    if init_tvec is None:
        init_tvec = (0, 0, 0)
    init_rvec = np.reshape(init_rvec, (3, 1))
    init_tvec = np.reshape(init_tvec, (3, 1))

    if verbose:
        print("Translation minimization:")
    cs_err, cs_tvec = estimate_target_translation(
        camera_points,
        sonar_points,
        range_resolution,
        angle_resolution,
        init_rvec,
        init_tvec,
        verbose,
    )
    # print("translation-only minimization: T = {}".format(cs_tvec))

    initial = [
        init_rvec[0],
        init_rvec[1],
        init_rvec[2],
        cs_tvec[0][0],
        cs_tvec[1][0],
        cs_tvec[2][0],
    ]

    if verbose:
        print("Full minimization:")
    cs_err, cs_rvec, cs_tvec = estimate_target_pose(
        camera_points,
        sonar_points,
        range_resolution,
        angle_resolution,
        initial,
        verbose,
    )
    # print("Full minimization: R = {}, T = {}".format(cs_rvec, cs_tvec))
    return cs_err, cs_rvec, cs_tvec
