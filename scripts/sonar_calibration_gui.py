#! /usr/bin/env python3

# Author: Laura Lindzey
# Copyright 2020-2022 University of Washington Applied Physics Laboratory
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.


import argparse
import cycler
import numpy as np
import os
import pickle
import sys

import cv2
import cv2.aruco

import rospy
import tf
import cv_bridge

from PyQt5 import QtGui, QtWidgets, QtCore

import matplotlib
import matplotlib.figure
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT

import imaging_sonar_calibration as isc

import marine_acoustic_msgs.msg

import charuco_utils


class NavigationToolbar(NavigationToolbar2QT):
    """
    Only display relevant buttons.
    Solution copied from:
    https://stackoverflow.com/questions/12695678/how-to-modify-the-navigation-toolbar-easily-in-a-matplotlib-figure-window
    """

    toolitems = [
        t
        for t in NavigationToolbar2QT.toolitems
        if t[0] in ("Home", "Pan", "Zoom", "Save")
    ]


class EnterPointDialog(QtWidgets.QDialog):
    def __init__(self, point_cb):
        super(EnterPointDialog, self).__init__()
        # This callback will be called with the label that should be removed
        self.setWindowTitle("Select Label")
        self.point_cb = point_cb
        self.setup_layout()

    def setup_layout(self):
        text_label = QtWidgets.QLabel("Label:")
        self.text_edit = QtWidgets.QLineEdit()
        ok_button = QtWidgets.QPushButton("OK")
        ok_button.clicked.connect(self.handle_ok)
        cancel_button = QtWidgets.QPushButton("Cancel")
        cancel_button.clicked.connect(self.handle_cancel)

        text_row = QtWidgets.QHBoxLayout()
        text_row.addWidget(text_label)
        text_row.addWidget(self.text_edit)

        button_row = QtWidgets.QHBoxLayout()
        button_row.addWidget(ok_button)
        button_row.addWidget(cancel_button)

        layout = QtWidgets.QVBoxLayout()
        layout.addLayout(text_row)
        layout.addLayout(button_row)
        self.setLayout(layout)

    def handle_cancel(self):
        # DO nothing, just close the window
        self.done(0)

    def handle_ok(self):
        self.point_cb(self.text_edit.text())
        self.done(0)


class AnnotatedCanvas(QtWidgets.QWidget):
    """Widget that gives canvas a title and a `?` with mouseover help"""

    def __init__(self, title_text, help_text, canvas):
        super(AnnotatedCanvas, self).__init__()

        title_label = QtWidgets.QLabel(title_text)

        help_button = QtWidgets.QPushButton("?")
        help_button.setDisabled(True)
        help_button.setToolTip(help_text)
        # TODO: This button is too big; I'd like to make it tight to the "?".
        # Ideally, it would calculated the desired height normally, then set width
        # to that, but I'm in a time crunch so taking this shortcut.
        help_button.setStyleSheet("padding: 3px;")

        title_row = QtWidgets.QHBoxLayout()
        title_row.addStretch(1)
        title_row.addWidget(title_label)
        title_row.addWidget(help_button)
        title_row.addStretch(1)

        layout = QtWidgets.QVBoxLayout()
        layout.addLayout(title_row)
        layout.addWidget(canvas)
        self.setLayout(layout)


class SensorWindow(QtWidgets.QMainWindow):
    def __init__(
        self, outdir, bagfile, camera_info_topic, camera_image_topic, sonar_image_topic
    ):
        super(SensorWindow, self).__init__()

        self.outdir = outdir

        self.camera_info_topic = camera_info_topic
        self.camera_image_topic = camera_image_topic
        self.sonar_image_topic = sonar_image_topic

        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)

        self.setup_layout()
        self.setup_callbacks()
        self.setup_data(bagfile)

        # Calls that assume more than one of the setup_* have been called
        self.handle_next_button()

        # Only called once; the rest of the draw* are called on every refresh
        self.plot_charuco_target(self.charuco_board, self.sonar_coords)

    def setup_data(self, bagfile):
        rospy.logwarn("SensorWindow.setup_data")
        self.data_gen = isc.generate_sensor_pairs(
            bagfile,
            self.camera_info_topic,
            self.camera_image_topic,
            self.sonar_image_topic,
        )
        self.bridge = cv_bridge.CvBridge()
        self.current_timestamp = None

        (
            self.good_timestamps,
            self.skip_timestamps,
            self.sonar_labels,
        ) = self.load_state()
        # Dict mapping SONAR timestamp to the charuco board's location in the
        # camera frame, where location is given as a (rvec, tvec) tuple.
        self.camera_poses = {}

        # TODO: Make board properties into parameters
        (
            self.aruco_dict,
            self.charuco_board,
            self.sonar_coords,
        ) = isc.init_charuco_sonar()

    def setup_layout(self):
        rospy.logwarn("SensorWindow.setup_layout")
        self.layout = QtWidgets.QVBoxLayout(self._main)

        ###################
        # Label row
        self.header_row = QtWidgets.QHBoxLayout()

        self.timestamp_label = QtWidgets.QLabel("Timestamp: None")
        self.good_label = QtWidgets.QLabel("Label: unknown")
        self.good_label.setAutoFillBackground(True)

        self.header_row.addStretch(5)
        self.header_row.addWidget(self.timestamp_label)
        self.header_row.addStretch(1)
        self.header_row.addWidget(self.good_label)
        self.header_row.addStretch(5)

        ###################
        # Matplotlib figures for displaying the Camera views

        # Figure showing the (idealized) charuco board + sonar targets
        self.target_fig = matplotlib.figure.Figure(figsize=(5, 3))
        self.target_ax = self.target_fig.add_axes([0, 0, 1, 1])
        self.target_ax.axis("off")
        self.target_artist = None
        self.target_canvas = FigureCanvas(self.target_fig)

        target_help = (
            "Charuco board used for the calibration. "
            "Sonar targets are installed at the center of "
            "the labeled black squares."
        )
        target_widget = AnnotatedCanvas("Target", target_help, self.target_canvas)

        # Figure showing the raw camera image
        self.raw_camera_fig = matplotlib.figure.Figure(figsize=(5, 3))
        self.raw_camera_ax = self.raw_camera_fig.add_axes([0, 0, 1, 1])
        self.raw_camera_ax.axis("off")
        self.raw_camera_artist = None
        self.raw_camera_canvas = FigureCanvas(self.raw_camera_fig)

        raw_camera_help = "Raw camera image from bagfile"
        raw_camera_widget = AnnotatedCanvas(
            "Raw Camera Image", raw_camera_help, self.raw_camera_canvas
        )

        # Figure showing aruco/charuco detections on the camera image
        self.charuco_annotated_camera_fig = matplotlib.figure.Figure(figsize=(5, 3))
        self.charuco_annotated_camera_ax = self.charuco_annotated_camera_fig.add_axes(
            [0, 0, 1, 1]
        )
        self.charuco_annotated_camera_ax.axis("off")
        self.charuco_annotated_camera_canvas = FigureCanvas(
            self.charuco_annotated_camera_fig
        )

        charuco_annotated_help = (
            "Detected aruco markers (green squares) and " "charuco corners (red dots)"
        )
        charuco_annotated_widget = AnnotatedCanvas(
            "Detected aruco / charuco",
            charuco_annotated_help,
            self.charuco_annotated_camera_canvas,
        )

        # Figure showing expected locations of the sonar targets on the
        # camera image, using the camera-derived board position
        self.camera_annotated_camera_fig = matplotlib.figure.Figure(figsize=(5, 3))
        self.camera_annotated_camera_ax = self.camera_annotated_camera_fig.add_axes(
            [0, 0, 1, 1]
        )
        self.camera_annotated_camera_ax.axis("off")
        self.camera_annotated_camera_canvas = FigureCanvas(
            self.camera_annotated_camera_fig
        )

        camera_camera_help = (
            "Red dots show the expected locations of sonar "
            "targets based on the detected charuco board"
        )
        camera_camera_widget = AnnotatedCanvas(
            "Charuco-derived locations",
            camera_camera_help,
            self.camera_annotated_camera_canvas,
        )

        # Figure showing expected locations of the sonar targets on the
        # camera image, using the sonar-derived board position
        self.camera_annotated_sonar_fig = matplotlib.figure.Figure(figsize=(5, 3))
        self.camera_annotated_sonar_ax = self.camera_annotated_sonar_fig.add_axes(
            [0, 0, 1, 1]
        )
        self.camera_annotated_sonar_ax.axis("off")
        self.camera_annotated_sonar_canvas = FigureCanvas(
            self.camera_annotated_sonar_fig
        )

        camera_sonar_help = (
            "Camera image annotated with arcs showing "
            "projection of labeled sonar points, \n"
            "using the current calibration.\n\n"
            "The displayed arc corresponds to a +/- 15 degree "
            "vertical field of view. \n"
            "The instrument's reported FOV is the angle at "
            "which the beam pattern drops by N dB. \n"
            "So, it is possible to see bright targets even "
            "if they are outside the nominal FOV.\n\n"
            "NB: The displayed image is NOT undistorted, "
            "but the sonar points have been projected onto "
            "the image using the CameraInfo."
        )
        camera_sonar_widget = AnnotatedCanvas(
            "Sonar-derived locations",
            camera_sonar_help,
            self.camera_annotated_sonar_canvas,
        )

        ###################
        # Matplotlib figures for displaying the sonar views

        # Polar plot of sonar data, with labels
        self.polar_sonar_fig = matplotlib.figure.Figure(figsize=(5, 3))
        self.polar_sonar_ax = self.polar_sonar_fig.add_axes(
            [0.05, 0.05, 0.9, 0.9], polar=True
        )
        self.polar_sonar_canvas = FigureCanvas(self.polar_sonar_fig)
        polar_sonar_help = "Polar plot of sonar image"
        polar_sonar_widget = AnnotatedCanvas(
            "Polar Sonar Image", polar_sonar_help, self.polar_sonar_canvas
        )

        # Figure showing expected locations of the sonar targets on the
        # sonar image, using the charuco-derived board position
        self.sonar_annotated_camera_fig = matplotlib.figure.Figure(figsize=(5, 3))
        self.sonar_annotated_camera_ax = self.sonar_annotated_camera_fig.add_axes(
            [0, 0, 1, 1]
        )
        self.sonar_annotated_camera_ax.axis("off")
        self.sonar_annotated_camera_canvas = FigureCanvas(
            self.sonar_annotated_camera_fig
        )
        sonar_camera_help = (
            "Expected locations of targets on sonar image, "
            "based on the charuco detection. \n\n"
            "Yellow circles use the input initial calibration. \n"
            "Red Xs use the currently-computed calibration."
        )
        sonar_camera_widget = AnnotatedCanvas(
            "Camera-derived locations",
            sonar_camera_help,
            self.sonar_annotated_camera_canvas,
        )

        transform_col = QtWidgets.QVBoxLayout()
        transform_col_label = QtWidgets.QLabel("COMPUTED TRANSFORMATIONS")
        self.charuco_pose_label = QtWidgets.QLabel("Camera -> Target: ")
        self.sonar_pose_label = QtWidgets.QLabel("Oculus -> Target: ")

        transform_col.addWidget(transform_col_label)
        transform_col.addWidget(self.charuco_pose_label)
        transform_col.addWidget(self.sonar_pose_label)

        ###################
        # Buttons for stepping through the data
        self.button_row = QtWidgets.QHBoxLayout()

        # For moving on to the next image pair, without labeling it "bad"
        self.next_button = QtWidgets.QPushButton("Next")
        self.next_button.setStyleSheet("padding: 3px;")
        self.next_button.clicked.connect(self.handle_next_button)
        # Skip 10 images
        self.next10_button = QtWidgets.QPushButton("Next10")
        self.next10_button.setStyleSheet("padding: 3px;")
        self.next10_button.clicked.connect(self.handle_next10_button)

        # Mark this image as "good" (saves to disk)
        self.good_button = QtWidgets.QPushButton("Mark Good")
        self.good_button.setStyleSheet("padding: 3px;")
        self.good_button.clicked.connect(self.handle_good_button)
        self.ungood_button = QtWidgets.QPushButton("UN Mark Good")
        self.ungood_button.setStyleSheet("padding: 3px;")
        self.ungood_button.clicked.connect(self.handle_unmark_good_button)

        # Updates the display to the next/prev data that had ben marked "good"
        # NB: there are two very confusing ways of stepping through the data.
        # 1) Sequentially, using next/skip/next10
        # 2) back/fwd through saved frames, using good/next/prev
        # clicking "next" will always jump you back to whatever comes out of
        # the bag file next; next/prev good buttons are always relative to
        # the current timestamp.
        self.next_good_button = QtWidgets.QPushButton("Next Good")
        self.next_good_button.setStyleSheet("padding: 3px;")
        self.next_good_button.clicked.connect(self.handle_next_good_button)
        self.prev_good_button = QtWidgets.QPushButton("Prev Good")
        self.prev_good_button.setStyleSheet("padding: 3px;")
        self.prev_good_button.clicked.connect(self.handle_prev_good_button)

        # Mark this image pair as unusable (don't show again)
        self.skip_button = QtWidgets.QPushButton("Skip")
        self.skip_button.setStyleSheet("padding: 3px;")
        self.skip_button.clicked.connect(self.handle_skip_button)

        # Remove labels
        self.remove_label_button = QtWidgets.QPushButton("Remove Label")
        self.remove_label_button.setStyleSheet("padding: 3px;")
        self.remove_label_button.clicked.connect(self.handle_remove_label_button)

        # Save all images as png
        self.print_button = QtWidgets.QPushButton("Print")
        self.print_button.setStyleSheet("padding: 3px;")
        self.print_button.clicked.connect(self.handle_print_button)

        self.button_row.addWidget(self.next_button)
        self.button_row.addWidget(self.next10_button)
        self.button_row.addWidget(self.good_button)
        self.button_row.addWidget(self.ungood_button)
        self.button_row.addWidget(self.prev_good_button)
        self.button_row.addWidget(self.next_good_button)
        self.button_row.addWidget(self.skip_button)
        self.button_row.addWidget(self.remove_label_button)
        self.button_row.addWidget(self.print_button)

        ###################
        # Matplotlib figures for displaying the sonar views

        # Main figure for labeling the sonar image
        self.sonar_image_fig = matplotlib.figure.Figure(figsize=(5, 3))
        self.sonar_image_ax = self.sonar_image_fig.add_axes([0.025, 0.025, 0.95, 0.95])
        self.sonar_image_artist = None
        self.sonar_image_canvas = FigureCanvas(self.sonar_image_fig)
        self.sonar_image_canvas.mpl_connect(
            "button_press_event", self.handle_sonar_click
        )
        self.sonar_image_canvas.mpl_connect(
            "button_release_event", self.handle_sonar_release
        )

        # Create the central column used for labeling the sonar targets.
        # Nothing within this layout needs resizing, so just use a layout
        # and wrap it in a widget to add to the main splitter.
        raw_sonar_help_button = QtWidgets.QPushButton("?")
        raw_sonar_help_button.setDisabled(True)
        help_text = (
            "Main display for labeling targets in sonar data.\n\n"
            "* Left-click to select a pixel, and enter corresponding "
            "label from the Target image. (e.g. A1)\n"
            "* Use Next/Prev buttons to step through the bagfile (10 will skip ahead by 10 images) \n"
            "* Click Skip to indicate that this image should never be shown again\n"
            "* Click Remove Label and then enter the ID (e.g. D5) to remove an annotation"
        )
        raw_sonar_help_button.setToolTip(help_text)

        raw_sonar_col = QtWidgets.QVBoxLayout()
        raw_sonar_col_header = QtWidgets.QHBoxLayout()
        raw_sonar_col_label = QtWidgets.QLabel("Raw Sonar Image")
        self.raw_sonar_col_toolbar = NavigationToolbar(self.sonar_image_canvas, self)
        raw_sonar_col_header.addWidget(raw_sonar_col_label)
        raw_sonar_col_header.addWidget(self.raw_sonar_col_toolbar)
        raw_sonar_col_header.addWidget(raw_sonar_help_button)
        raw_sonar_col.addLayout(raw_sonar_col_header, stretch=0)
        raw_sonar_col.addWidget(self.sonar_image_canvas, stretch=1)
        raw_sonar_col.addLayout(self.button_row)

        # Create the left-most column.
        # Since we want a agrid of annotated camera images, need to nest splitters
        aruco_splitter = QtWidgets.QSplitter()
        aruco_splitter.setOrientation(QtCore.Qt.Orientation.Horizontal)
        aruco_splitter.addWidget(raw_camera_widget)
        aruco_splitter.addWidget(charuco_annotated_widget)

        reproj_camera_splitter = QtWidgets.QSplitter()
        reproj_camera_splitter.setOrientation(QtCore.Qt.Orientation.Horizontal)
        reproj_camera_splitter.addWidget(camera_camera_widget)
        reproj_camera_splitter.addWidget(camera_sonar_widget)

        camera_splitter = QtWidgets.QSplitter()
        camera_splitter.setOrientation(QtCore.Qt.Orientation.Vertical)
        camera_splitter.addWidget(target_widget)
        camera_splitter.addWidget(aruco_splitter)
        camera_splitter.addWidget(reproj_camera_splitter)
        transform_widget = QtWidgets.QWidget()
        transform_widget.setLayout(transform_col)
        camera_splitter.addWidget(transform_widget)

        sonar_splitter = QtWidgets.QSplitter()
        sonar_splitter.setOrientation(QtCore.Qt.Orientation.Vertical)
        sonar_splitter.addWidget(polar_sonar_widget)
        sonar_splitter.addWidget(sonar_camera_widget)
        self.sonar_err_label = QtWidgets.QLabel("Reprojection Error: ")
        sonar_splitter.addWidget(self.sonar_err_label)

        # Finally, add all columns to the main window!
        main_splitter = QtWidgets.QSplitter()
        main_splitter.setOrientation(QtCore.Qt.Orientation.Horizontal)
        main_splitter.addWidget(camera_splitter)
        label_sonar_widget = QtWidgets.QWidget()
        label_sonar_widget.setLayout(raw_sonar_col)
        main_splitter.addWidget(label_sonar_widget)
        main_splitter.addWidget(sonar_splitter)

        self.layout.addLayout(self.header_row)
        self.layout.addWidget(main_splitter, stretch=1)

    def plot_charuco_target(self, charuco_board, sonar_coords):
        """
        Draw charuco board on the target_ax.

        I like having data input be explicit, because it makes it easier to
        reason about ordering dependencies in the init steps.
        """
        square_pixels = 100
        ncols, nrows = charuco_board.getChessboardSize()
        xpixels = ncols * square_pixels
        ypixels = nrows * square_pixels
        board_img = charuco_board.draw((xpixels, ypixels))
        self.target_ax.imshow(
            board_img, cmap="gray", aspect="equal", interpolation="none"
        )

        scale = square_pixels / charuco_board.getSquareLength()
        for label, coord in sonar_coords.items():
            xx, yy = coord
            x = xx * scale
            # coordinate system conversion: board's origin is lower-left,
            # but image's is upper-left.
            y = ypixels - yy * scale
            self.target_ax.text(
                x,
                y,
                label,
                color="white",
                fontsize=14,
                horizontalalignment="center",
                verticalalignment="center",
            )

    def plot_raw_camera_data(self, camera_data):
        """
        Update the figure that shows the bare camera image.

        Since it was easy in this case, save the artist to help make
        updates faster.
        """
        if self.raw_camera_artist is None:
            self.raw_camera_artist = self.raw_camera_ax.imshow(camera_data, cmap="gray")
        else:
            self.raw_camera_artist.set_data(camera_data)
        self.raw_camera_canvas.draw()

    def plot_charuco_detections(
        self, camera_data, aruco_corners, aruco_ids, charuco_corners, charuco_ids
    ):
        """
        Update the figure that shows detected aruco markers and charuco
        corners on top of the camera data.

        * aruco_corners -- np.ndarray (empty for no data)
        * charuco_corners -- None or array (None for no data)

        NB: It's not my fault that these have different forms for "no data" ...
            that can be traced back to the function signatures of cv2.aruco's
            detectMarkers vs. interpolateCornersCharuco
        """
        self.charuco_annotated_camera_ax.cla()
        self.charuco_annotated_camera_ax.axis("off")
        self.charuco_annotated_camera_ax.imshow(camera_data, cmap="gray")

        if len(aruco_corners) > 0:
            for corner, _corner_id in zip(aruco_corners, aruco_ids):
                pt1, pt2, pt3, pt4 = corner[0]
                xx, yy = zip(pt1, pt2, pt3, pt4, pt1)
                self.charuco_annotated_camera_ax.plot(xx, yy, "c")
                # Removed because too noisy in GUI -- only useful on saved images.
                # self.charuco_annotated_camera_ax.text(np.mean(xx), np.mean(yy),
                #                                      "{}".format(corner_id[0]), color='c')
        if charuco_corners is not None:
            for corner, _corner_id in zip(charuco_corners, charuco_ids):
                xx, yy = corner[0]
                circle = matplotlib.patches.Circle((xx, yy), 5, color="r")
                self.charuco_annotated_camera_ax.add_artist(circle)
                # Removed because too noisy in GUI -- only useful on saved images.
                # self.charuco_annotated_camera_ax.text(
                #         xx+10, yy+10, "{}".format(corner_id[0]), color='r')

        self.charuco_annotated_camera_canvas.draw()

    def plot_camera_targets_from_sonar(
        self, camera_data, camera_info, cs_rot, cs_trans
    ):
        """
        Update the figure that shows the position of the labeled sonar
        targets superimposed on the camera data, using the calculated
        camera/sonar transformation.

        * cs_rot -- rotation matrix from sonar to camera frames
        * cs_trans -- translation vector from sonar to camera frames
        """
        self.camera_annotated_sonar_ax.cla()
        self.camera_annotated_sonar_ax.axis("off")
        self.camera_annotated_sonar_ax.imshow(camera_data, cmap="gray")

        # color_cycler = cycler.cycler(color=matplotlib.cm.inferno(np.linspace(0,1,10)))
        color_cycler = cycler.cycler(color=matplotlib.cm.viridis(np.linspace(0, 1, 10)))
        my_cycler = color_cycler()

        if self.current_timestamp in self.sonar_labels:
            points = self.sonar_labels[self.current_timestamp]
            for label, coord in points.items():
                azi_deg, rr = coord
                azi_rad = np.radians(azi_deg)
                elev_rads = np.arange(
                    np.radians(-15.0), np.radians(15.0), np.radians(0.25)
                )
                label_color = next(my_cycler)["color"]
                for elev_rad in elev_rads:
                    # X is forward
                    zz = rr * np.sin(elev_rad)
                    xx = rr * np.cos(elev_rad) * np.cos(azi_rad)
                    yy = rr * np.cos(elev_rad) * np.sin(azi_rad)
                    sonar_point = np.reshape([xx, yy, zz], (3, 1))
                    # cs_{trans, rot} give transformation from camera to sonar frame
                    # We need the opposite here ...
                    camera_coord = np.transpose(cs_rot) @ (sonar_point - cs_trans)

                    camera_matrix = np.reshape(camera_info.K, (3, 3))
                    image_coord, _ = cv2.projectPoints(
                        camera_coord,
                        0 * cs_trans,
                        0 * cs_trans,
                        camera_matrix,
                        camera_info.D,
                    )
                    ix, iy = image_coord[0][0]
                    # don't plot points outside the FOV
                    nrows, ncols = camera_data.shape
                    if ix >= 0 and ix < ncols and iy >= 0 and iy < nrows:
                        self.camera_annotated_sonar_ax.plot(
                            ix, iy, ".", ms=1, color=label_color
                        )

                # Intentionally plot the label for the last point drawn
                self.camera_annotated_sonar_ax.text(ix, iy, label, color=label_color)

        self.camera_annotated_sonar_canvas.draw()

    def plot_camera_targets_from_camera(self, camera_data, camera_info, rvec, tvec):
        """
        Update the figure that shows the inferred position of the sonar
        targets superimposed on the camera data.

        This serves to check that I have the transformations correct from
        coordinates on the target to the image frame.
        """
        self.camera_annotated_camera_ax.cla()
        self.camera_annotated_camera_ax.axis("off")
        self.camera_annotated_camera_ax.imshow(camera_data, cmap="gray")

        if rvec is not None:
            rot, _ = cv2.Rodrigues(rvec)
            camera_matrix = np.reshape(camera_info.K, (3, 3))
            pose_text = (
                "Camera -> Target: \n"
                "rvec = [{:.2f}, {:.2f}, {:.2f}] \n"
                "tvec = [{:.2f}, {:.2f}, {:.2f}] \n"
                "fwd = {:.2f}, right = {:.2f}, down = {:.2f}".format(
                    rvec[0][0],
                    rvec[1][0],
                    rvec[2][0],
                    tvec[0][0],
                    tvec[1][0],
                    tvec[2][0],
                    tvec[2][0],
                    tvec[0][0],
                    tvec[1][0],
                )
            )
            self.charuco_pose_label.setText(pose_text)
            for _label, (cx, cy) in self.sonar_coords.items():
                target_point = np.reshape([cx, cy, 0], (3, 1))
                # Option 1: object points relative to the board, using
                # rvec/tvec from estimatePose to transform into camera frame
                # image_coord, _  = cv2.projectPoints(object_points, rvec, tvec,
                #                                    camera_matrix, camera_info.D)
                # xx, yy = image_coord[0][0]
                # Option 2: use rvec tvec to transform points into camera frame,
                # project only corrects for distortion
                world_coord = tvec + rot @ target_point
                image_coord, _ = cv2.projectPoints(
                    world_coord, 0 * rvec, 0 * tvec, camera_matrix, camera_info.D
                )
                xx, yy = image_coord[0][0]
                # don't plot points outside the FOV
                nrows, ncols = camera_data.shape
                if xx >= 0 and xx < ncols and yy >= 0 and yy < nrows:
                    self.camera_annotated_camera_ax.plot(xx, yy, "r.")
                    # Removed because too noisy in GUI -- only useful on saved images.
                    # self.camera_annotated_camera_ax.text(xx, yy, label, color='black')
        else:
            self.charuco_pose_label.setText("Camera -> Target: None")

        self.camera_annotated_camera_canvas.draw()

    def plot_sonar_image(self, data, extent, rvec, tvec, keep_limits):
        """
        Plot sonar image in rectangular plot.
        This is the axis that will be used for human annotations.
        """
        xlim = self.sonar_image_ax.get_xlim()
        ylim = self.sonar_image_ax.get_ylim()
        self.sonar_image_ax.cla()

        self.sonar_image_ax.imshow(
            data,
            cmap="inferno",
            vmin=0,
            vmax=128,
            extent=extent,
            aspect="auto",
            interpolation="none",
            origin="lower",
        )

        # Plot the human-provided labels
        if self.current_timestamp in self.sonar_labels:
            points = self.sonar_labels[self.current_timestamp]
            for _label, coord in points.items():
                theta_deg, rr = coord
                self.sonar_image_ax.plot(
                    theta_deg, rr, marker="o", ms=8, c="white", fillstyle="none"
                )

        # Plot the projected location of the targets
        if rvec is not None:
            rot, _ = cv2.Rodrigues(rvec)
            # Project points in the charuco board's coordinate frame
            for label, (cx, cy) in self.sonar_coords.items():
                target_point = np.reshape([cx, cy, 0], (3, 1))
                pt_sf = tvec + rot @ target_point  # sonar instrument frame
                pt_if = isc.image_from_sonar3d(pt_sf)  # sonar image frame
                th_deg = np.degrees(pt_if[0])
                rr = pt_if[1]
                min_th, max_th, min_range, max_range = extent
                theta_in_range = (th_deg >= min_th) and (th_deg <= max_th)
                range_in_range = (rr >= min_range) and (rr <= max_range)
                if theta_in_range and range_in_range:
                    self.sonar_image_ax.plot(th_deg, rr, "g.", markersize=4)
                    self.sonar_image_ax.text(th_deg, rr, label, c="white", fontsize=6)

        if keep_limits:
            self.sonar_image_ax.set_xlim(xlim)
            self.sonar_image_ax.set_ylim(ylim)
        self.sonar_image_canvas.draw()

    def plot_polar_sonar_image(self, data):
        """
        Plot sonar image in polar plot, including the annotated points.

        Degrees/Radians is annoyingly inconsistent here.
        Min/max are specified in degrees.
        Actually plotting data, angles need to be in radians.
        """
        self.polar_sonar_ax.cla()
        theta, radius = np.meshgrid(
            self.sonar_image.azimuth_angles, self.sonar_image.ranges
        )
        self.polar_sonar_ax.pcolormesh(
            theta, radius, data, cmap="inferno", vmin=0, vmax=128
        )
        self.polar_sonar_ax.set_thetamin(
            np.degrees(np.min(self.sonar_image.azimuth_angles))
        )
        self.polar_sonar_ax.set_thetamax(
            np.degrees(np.max(self.sonar_image.azimuth_angles))
        )
        self.polar_sonar_ax.set_theta_zero_location("N")
        self.polar_sonar_ax.set_theta_direction(-1)
        self.polar_sonar_ax.patch.set_facecolor("black")
        # TODO: This updated the image, but with weird artifacts.
        #    self.polar_sonar_artist.set_array(sonar_matrix.flatten())

        # Commenting these out for now because the labels obscure the image when it's small.
        # if self.current_timestamp in self.sonar_labels:
        #     points = self.sonar_labels[self.current_timestamp]
        #     for label, coord in points.items():
        #         theta, rr = coord
        #         self.polar_sonar_ax.plot(np.radians(theta), rr, '.', color='green', markersize=2)
        #         self.polar_sonar_ax.text(np.radians(theta), rr, label, fontsize=6, color='white')

        self.polar_sonar_canvas.draw()

    def plot_sonar_targets_from_camera(self, data, rvec, tvec, cam_rvec, cam_tvec, err):
        self.sonar_annotated_camera_ax.cla()

        min_th = self.sonar_image.azimuth_angles[0]
        max_th = self.sonar_image.azimuth_angles[-1]
        min_range = self.sonar_image.ranges[0]
        max_range = self.sonar_image.ranges[-1]
        extent_radians = [min_th, max_th, min_range, max_range]

        self.sonar_annotated_camera_ax.imshow(
            data,
            cmap="inferno",
            vmin=0,
            vmax=128,
            extent=extent_radians,
            aspect="auto",
            interpolation="none",
            origin="lower",
        )

        target_points = np.transpose(
            np.array([[coord[0], coord[1], 0] for coord in self.sonar_coords.values()])
        )

        if rvec is not None:
            print("Using calibration data from this frame")
            rot, _ = cv2.Rodrigues(rvec)
            label_text = (
                "Sonar -> Target: \n"
                "rvec = [{:.2f}, {:.2f}, {:.2f}]\n"
                "tvec = [{:.2f}, {:.2f}, {:.2f}]\n"
                "fwd = {:.2f}, right = {:.2f}, down = {:.2f}".format(
                    rvec[0][0],
                    rvec[1][0],
                    rvec[2][0],
                    tvec[0][0],
                    tvec[1][0],
                    tvec[2][0],
                    tvec[0][0],
                    tvec[1][0],
                    tvec[2][0],
                )
            )
            self.sonar_pose_label.setText(label_text)
            sonar_points = tvec + rot @ target_points
            sonar_coords = isc.image_from_sonar3d(sonar_points)
            self.sonar_annotated_camera_ax.plot(
                sonar_coords[0, :], sonar_coords[1, :], "rx", fillstyle="none"
            )
        elif cam_rvec is not None:
            print("Using calibration data from another frame")
            # TODO: Actually *always* show the aggregate calibration?
            #   (This requires actually calculating one from more than one frame...)
            # Use calibration value from another frame ... just to show that it generalizes.

            # Final ROS XYZ: 0.006632 -0.055447 0.075904
            # Final ROS YPR: -3.101167 -1.401133
            # [1.33891995, 1.32590053, 1.1214456 ]
            cam_rot, _ = cv2.Rodrigues(cam_rvec)
            init_rvec = np.reshape([1.33891995, 1.32590053, 1.1214456], (3, 1))
            init_rot, _ = cv2.Rodrigues(init_rvec)
            init_tvec = np.reshape([0.006632, -0.055447, 0.075904], (3, 1))
            init_sonar_points = init_tvec + init_rot @ (
                cam_tvec + cam_rot @ target_points
            )
            init_sonar_coords = isc.image_from_sonar3d(init_sonar_points)
            self.sonar_annotated_camera_ax.plot(
                init_sonar_coords[0, :], init_sonar_coords[1, :], "rx", fillstyle="none"
            )

        # This is an ugly set of magic numbers...
        # In order to plot the "as-initialized", use the camera pose + initial
        # camera->sonar transformation.
        if cam_rvec is not None:
            cam_rot, _ = cv2.Rodrigues(cam_rvec)
            init_rvec = np.reshape([1.2092, 1.2092, 1.2092], (3, 1))
            init_rot, _ = cv2.Rodrigues(init_rvec)

            # Plot prior that camera and sonar are aligned
            # init_tvec = np.reshape([0, 0, 0], (3,1))
            # init_sonar_points = init_tvec + init_rot @ (cam_tvec + cam_rot @ target_points)
            # init_sonar_coords = isc.image_from_sonar3d(init_sonar_points)
            # self.sonar_annotated_camera_ax.plot(init_sonar_coords[0,:],
            #                                    init_sonar_coords[1,:],
            #                                    'go', fillstyle='none')

            # This uses the values from the URDF
            init_tvec = np.reshape([0, -0.06, 0.095], (3, 1))
            init_sonar_points = init_tvec + init_rot @ (
                cam_tvec + cam_rot @ target_points
            )
            init_sonar_coords = isc.image_from_sonar3d(init_sonar_points)
            self.sonar_annotated_camera_ax.plot(
                init_sonar_coords[0, :], init_sonar_coords[1, :], "yo", fillstyle="none"
            )

            self.sonar_annotated_camera_ax.set_xlim(extent_radians[0:2])
            self.sonar_annotated_camera_ax.set_ylim(extent_radians[2:4])

        self.sonar_err_label.setText("Reprojection error: {:.2f}".format(err))
        self.sonar_annotated_camera_canvas.draw()

    def handle_next_button(self):
        try:
            # TODO: Our handling of camera info is sketchy: it is NOT being
            #   saved alongside the images. This is OK for now, since it's
            # fixed throughout the bagfiles, but should be revised
            # if/when I go back and change how we step through the data.
            self.sonar_image, self.camera_info, image_mono = next(self.data_gen)
            while self.sonar_image.header.stamp.to_sec() in self.skip_timestamps:
                time_sec = self.sonar_image.header.stamp.to_sec()
                print("Skipping timestamp: {}".format(time_sec))
                self.sonar_image, _, image_mono = next(self.data_gen)
        except Exception as ex:
            # We expect to hit this at the end of the bagfile.
            print(ex)
        self.current_timestamp = self.sonar_image.header.stamp.to_sec()

        # NB: We don't every explicitly undistort the image for display;
        #     instead, any calls projecting aruco markers to/from the image
        #     use camera_info to do the conversion to image coordinates.
        self.camera_data = self.bridge.imgmsg_to_cv2(image_mono, "mono8")

        # This is duplicated in update_plots, but I want to automatically skip
        # frames that don't have sufficient charuco tags.
        # aruco_corners, aruco_ids, charuco_corners, charuco_ids, camera_rvec, camera_tvec = self._detect_charuco()
        # if camera_rvec is None:
        #     print("Automatically skipping.")
        #     self.handle_skip_button()
        # else:
        #     self.update_plots(False)
        self.update_plots(False)

    def handle_next10_button(self):
        """
        Useful for fast-forwarding through the data.
        """
        # Skip forward 9 frames then handle the next one.
        # NB -- does not skip additional frames is some of the fast-forwarded
        #    ones were hand-labeled as "skip"
        for _ in range(9):
            _ = next(self.data_gen)
        self.handle_next_button()

    def handle_good_button(self):
        self.good_timestamps.add(self.current_timestamp)
        # NB: using the _camera's_ timestamp for both of 'em, for easier playback.
        camera_filename = "{}/{}_camera.png".format(self.outdir, self.current_timestamp)
        sonar_filename = "{}/{}_sonar.bin".format(self.outdir, self.current_timestamp)
        print("Trying to save data to {}".format(camera_filename))
        if not os.path.exists(camera_filename):
            cv2.imwrite(camera_filename, self.camera_data)
        if not os.path.exists(sonar_filename):
            with open(sonar_filename, "wb") as fp:
                # NB: Pickle does not play nicely with the rosbag API, since
                #     rosbag relies on duck typing. You can re-load the pickled
                #     file from the same process, but attempts to load it from a
                #     different one will always fail with an error like:
                #     > ModuleNotFoundError: No module named 'tmpwujkj9ik'
                # So, use rosmsg's serialization/deserialization to save images
                self.sonar_image.serialize(fp)

        self.save_state()
        self.update_good_label()

    def handle_unmark_good_button(self):
        self.good_timestamps.discard(self.current_timestamp)
        self.save_state()
        self.update_good_label()

    def load_from_timestamp(self, timestamp):
        camera_filename = "{}/{}_camera.png".format(self.outdir, timestamp)
        sonar_filename = "{}/{}_sonar.bin".format(self.outdir, timestamp)

        try:
            camera_data = cv2.imread(camera_filename, cv2.IMREAD_GRAYSCALE)
        except Exception as ex:
            print(
                "Could not load camera data for timestamp: {}, file: {}".format(
                    timestamp, camera_filename
                )
            )
            print(ex)
            return

        try:
            with open(sonar_filename, "rb", buffering=0) as fp:
                sonar_image = marine_acoustic_msgs.msg.SonarImage()
                data = fp.readall()
                sonar_image.deserialize(data)
        except Exception as ex:
            print(
                "Could not load sonar data for timestamp: {}, file: {}".format(
                    timestamp, sonar_filename
                )
            )
            print(ex)
            return

        # Both reads succeeded, so we can set data!
        self.current_timestamp = timestamp
        self.camera_data = camera_data
        self.sonar_image = sonar_image

        print(
            "load_from_timestamp: camera data shape {}".format(self.camera_data.shape)
        )

    def handle_next_good_button(self):
        ts_array = np.array(list(self.good_timestamps))
        (future_idxs,) = np.where(ts_array > self.current_timestamp)
        if future_idxs.size == 0:
            print("No future data has been labeled good")
            return
        next_timestamp = np.min(ts_array[future_idxs])

        self.load_from_timestamp(next_timestamp)
        self.update_plots(False)

    def handle_prev_good_button(self):
        ts_array = np.array(list(self.good_timestamps))
        (past_idxs,) = np.where(ts_array < self.current_timestamp)
        if past_idxs.size == 0:
            print("No previous data has been labeled good")
            return
        prev_timestamp = np.max(ts_array[past_idxs])
        self.load_from_timestamp(prev_timestamp)
        self.update_plots(False)

    def handle_skip_button(self):
        self.skip_timestamps.add(self.current_timestamp)
        self.save_state()
        self.handle_next_button()

    def handle_print_button(self):
        print("Saving plots!")
        target_filename = "{}/{}_target.png".format(self.outdir, self.current_timestamp)
        self.target_fig.savefig(target_filename)

        raw_camera_filename = "{}/{}_raw_camera.png".format(
            self.outdir, self.current_timestamp
        )
        self.raw_camera_fig.savefig(raw_camera_filename)

        aruco_annotations_filename = "{}/{}_aruco_annotations.png".format(
            self.outdir, self.current_timestamp
        )
        self.charuco_annotated_camera_fig.savefig(aruco_annotations_filename)

        camera_camera_filename = "{}/{}_camera_camera.png".format(
            self.outdir, self.current_timestamp
        )
        self.camera_annotated_camera_fig.savefig(camera_camera_filename)

        camera_sonar_filename = "{}/{}_camera_sonar.png".format(
            self.outdir, self.current_timestamp
        )
        self.camera_annotated_sonar_fig.savefig(camera_sonar_filename)

        sonar_camera_filename = "{}/{}_sonar_camera.png".format(
            self.outdir, self.current_timestamp
        )
        self.sonar_annotated_camera_fig.savefig(sonar_camera_filename)

        polar_sonar_filename = "{}/{}_polar_sonar.png".format(
            self.outdir, self.current_timestamp
        )
        self.polar_sonar_fig.savefig(polar_sonar_filename)

        raw_sonar_filename = "{}/{}_raw_sonar.png".format(
            self.outdir, self.current_timestamp
        )
        self.sonar_image_fig.savefig(raw_sonar_filename)

    def handle_remove_label_button(self):
        dialog = EnterPointDialog(self.remove_point)
        target_filename = "{}/{}_target.png".format(self.outdir, self.current_timestamp)
        self.target_fig.savefig(target_filename)
        dialog.exec_()

    def remove_point(self, text):
        if self.current_timestamp in self.sonar_labels:
            label = text.strip().upper()
            # Technically, pop doesn't require checking if the dict contains
            # the key, but we only want to redraw if necessary.
            if label in self.sonar_labels[self.current_timestamp]:
                self.sonar_labels[self.current_timestamp].pop(label, None)
                self.save_state()
                self.update_plots()

    def handle_sonar_click(self, event):
        """
        Store the location of the previous click in order to determine
        whether the release is likely to have belonged to a pan/zoom and should be ignored
        """
        self.click_event = event

    def handle_sonar_release(self, event):
        """
        If this mouse release did not correspond to a drag, it's probably meant
        to label the image (rather than resize), so pop up dialog input a label.
        """
        if (
            abs(self.click_event.x - event.x) > 2
            or abs(self.click_event.y - event.y) > 2
        ):
            return
        dialog = EnterPointDialog(lambda x, event=event: self.add_point(event, x))
        dialog.exec_()

    def add_point(self, event, text):
        label = text.strip().upper()
        if label not in self.sonar_coords.keys():
            print("Cannot add point {}; not valid label".format(label))
            return

        if self.current_timestamp not in self.sonar_labels:
            self.sonar_labels[self.current_timestamp] = {}

        self.sonar_labels[self.current_timestamp][label] = (event.xdata, event.ydata)
        self.save_state()
        self.update_plots()

    def update_good_label(self):
        palette = QtGui.QPalette()
        if self.current_timestamp in self.good_timestamps:
            palette.setColor(QtGui.QPalette.Window, QtGui.QColor("green"))
            self.good_label.setText("label: Good")
        else:
            palette.setColor(QtGui.QPalette.Window, QtGui.QColor("gray"))
            self.good_label.setText("label: Unknown")
        self.good_label.setPalette(palette)

    def get_plottable_sonar(self):
        # type: None -> (np.ndarray, List]
        """
        Reshape the most recent sonar image into the right format to
        sent to plt.imshow, and determine the appropriate extent.

        * sonar_matrix: intensities
        * extent_degrees: [min_th, max_th, min_range, max_range]
        """
        assert self.sonar_image.data_size == 1
        # Convert from bytes in message field to numpy array
        n_ranges = len(self.sonar_image.ranges)
        n_angles = len(self.sonar_image.azimuth_angles)
        sonar_matrix = np.array(
            [bb for bb in self.sonar_image.intensities]  # noqa: C416
        )
        sonar_matrix = np.reshape(sonar_matrix, (n_ranges, n_angles))

        # Annoyingly, polar plots in matplotlib live in degrees,
        # So before plotting, I'm converting everything to degrees.
        # This includes the labeled points...
        extent_degrees = [
            np.degrees(self.sonar_image.azimuth_angles[0]),
            np.degrees(self.sonar_image.azimuth_angles[-1]),
            self.sonar_image.ranges[0],
            self.sonar_image.ranges[-1],
        ]

        return sonar_matrix, extent_degrees

    # TODO: Finish pulling utility functions out. Above this line is clean...

    # There are several different ways of doing the minimization, so pull them
    # out into their own functions.
    def calibrate_sonar(self, camera_rvec, camera_tvec):
        # type: None ->
        """
        I've found that its more stable to direcly solve for the camera->sonar
        transformation than to try to solve for the sonar->board transform
        and then chain them. I think this is because we can start the former
        optimization with a much better prior, so it's likely to land in the
        correct local minimum.

        I've also found it more stable if we first solve for the translation,
        holding rotation constant, and then solve for the full pose.

        * sonar_points: locations of the detected points in the sonar frame
        * cs_rvec: rotation to align
        * cs_tvec: vector from camera frame to sonar frame

        """
        have_labels = self.current_timestamp in self.sonar_labels
        have_camera = camera_rvec is not None
        if not have_labels or not have_camera:
            return None, None, None, None, None, None

        labeled_points = self.sonar_labels[self.current_timestamp]
        sonar_points, target_points = isc.get_sonar_target_correspondences(
            labeled_points
        )

        # Transform from target's coordinate frame to camera coordinate frame
        camera_rot, _ = cv2.Rodrigues(camera_rvec)
        camera_points = camera_tvec + camera_rot @ target_points

        range_resolution, angle_resolution = isc.get_sonar_resolution(self.sonar_image)

        # TODO: These initial vectors should become input params!
        init_rvec = (1.2092, 1.2092, 1.2092)
        init_tvec = (0, -0.06, 0.095)  # I think this is from the CAD drawing

        cs_err, cs_rvec, cs_tvec = isc.calibrate_sonar(
            sonar_points,
            camera_points,
            range_resolution,
            angle_resolution,
            init_rvec,
            init_tvec,
        )

        cs_rot, _ = cv2.Rodrigues(cs_rvec)
        yaw, pitch, roll = tf.transformations.euler_from_matrix(np.transpose(cs_rot))
        dx, dy, dz = cs_tvec[0][0], cs_tvec[1][0], cs_tvec[2][0]
        print("Final ROS XYZ: {:03f} {:03f} {:03f}".format(dx, dy, dz))
        print("Final ROS YPR: {:03f} {:03f} {:03f}".format(yaw, pitch, roll))
        print()

        # TODO: Calculate sonar-> target from camera->sonar and camera->target
        camera_rot, _ = cv2.Rodrigues(camera_rvec)
        sonar_tvec = cs_tvec + cs_rot @ camera_tvec
        sonar_rot = cs_rot @ camera_rot
        sonar_rvec, _ = cv2.Rodrigues(sonar_rot)

        return sonar_points, target_points, cs_rvec, cs_tvec, sonar_rvec, sonar_tvec

    def update_plots(self, keep_limits=True):
        print("Showing sonar data at timestamp: {}".format(self.current_timestamp))
        self.timestamp_label.setText(
            "Sonar timestamp: {:02f}".format(self.current_timestamp)
        )

        self.update_good_label()

        cc = charuco_utils.detect_charuco_board(
            self.aruco_dict, self.charuco_board, self.camera_data, self.camera_info
        )
        (
            aruco_corners,
            aruco_ids,
            charuco_corners,
            charuco_ids,
            camera_rvec,
            camera_tvec,
        ) = cc
        # Save these! The current timestamp is actually the SONAR's header
        # stamp, but that's what we're using to index into the sensor pairs.
        if camera_rvec is not None:
            self.camera_poses[self.current_timestamp] = (camera_rvec, camera_tvec)

        ######################
        # Update the figures

        self.plot_raw_camera_data(self.camera_data)
        self.plot_charuco_detections(
            self.camera_data, aruco_corners, aruco_ids, charuco_corners, charuco_ids
        )
        self.plot_camera_targets_from_camera(
            self.camera_data, self.camera_info, camera_rvec, camera_tvec
        )

        sonar_matrix, extent_degrees = self.get_plottable_sonar()

        cc = self.calibrate_sonar(camera_rvec, camera_tvec)
        sonar_points, target_points, cs_rvec, cs_tvec, sonar_rvec, sonar_tvec = cc

        self.plot_sonar_image(
            sonar_matrix,
            extent_degrees,
            sonar_rvec,
            sonar_tvec,
            keep_limits=keep_limits,
        )
        self.plot_polar_sonar_image(sonar_matrix)
        if sonar_rvec is not None:
            rres, ares = isc.get_sonar_resolution(self.sonar_image)
            sonar_err = isc.calc_projection_error(
                target_points, sonar_points, sonar_rvec, sonar_tvec, rres, ares
            )
        else:
            sonar_err = -1.0

        if cs_rvec is None:
            cs_rotation = None
        else:
            cs_rotation, _ = cv2.Rodrigues(cs_rvec)
        self.plot_camera_targets_from_sonar(
            self.camera_data, self.camera_info, cs_rotation, cs_tvec
        )

        self.plot_sonar_targets_from_camera(
            sonar_matrix, sonar_rvec, sonar_tvec, camera_rvec, camera_tvec, sonar_err
        )

    def setup_callbacks(self):
        pass

    def load_state(self):
        filename = "{}/calibration_labels.pkl".format(self.outdir)
        if os.path.exists(filename):
            try:
                with open(filename, "rb") as fp:
                    data = pickle.load(fp)
                # backwards-compatible with previous format
                # TODO: Maybe move this out of the try/except block?
                if "good_timestamps" in data:
                    good = data["good_timestamps"]
                else:
                    good = set()
                return good, data["skip_timestamps"], data["sonar_labels"]

            except Exception as ex:
                print("Could not load labels from: {}".format(filename))
                print("")
                # Intentionally not trying to recover, since we don't wan't
                # to accidentally overwrite a file that the human has
                # already started!
                raise (ex)
        else:
            return set(), set(), {}

    def save_state(self):
        """
        Save all of the human-generated metadata.
        For now, this is the skipped/skipped timestamps and the labeled points.

        Other possibly-relevant state data:
        * Generate index of bagfile to enable stepping through backwards
        * Actually save the charuco board locations so the pickle files are
          sufficient for running calibration independently.
        """
        filename = "{}/calibration_labels.pkl".format(self.outdir)
        with open(filename, "wb") as fp:
            pickle.dump(
                {
                    "good_timestamps": self.good_timestamps,
                    "skip_timestamps": self.skip_timestamps,
                    "sonar_labels": self.sonar_labels,
                },
                fp,
            )

        cam_filename = "{}/camera_poses.pkl".format(self.outdir)
        with open(cam_filename, "wb") as fp:
            pickle.dump(self.camera_poses, fp)


if __name__ == "__main__":
    description = "GUI for manually labeling imaging sonar calibration data"
    parser = argparse.ArgumentParser(description=description)
    outdir_help = (
        "name of file to save manually generated labels. If file "
        "already exists, will resume from previous session"
    )
    parser.add_argument("outdir", help=outdir_help)
    parser.add_argument("bagfile", help="Path to single bagfile")

    args = parser.parse_args()

    try:
        if not os.path.exists(args.outdir):
            os.makedirs(args.outdir)
    except Exception as ex:
        print("Could not create directory: {}".format(args.outdir))
        print("")
        raise (ex)

    import signal

    signal.signal(signal.SIGINT, signal.SIG_DFL)

    # sensor_msgs/CameraInfo
    camera_info_topic = "/raven/stereo/left/camera_info"
    # sensor_msgs/Image
    camera_image_topic = "/raven/stereo/left/image_raw"
    # marine_acoustic_msgs/SonarImage
    sonar_image_topic = "/raven/oculus/sonar_image"

    app = QtWidgets.QApplication(sys.argv)
    window = SensorWindow(
        args.outdir,
        args.bagfile,
        camera_info_topic,
        camera_image_topic,
        sonar_image_topic,
    )
    window.show()
    sys.exit(app.exec_())
